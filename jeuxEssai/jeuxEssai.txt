﻿Les observations de ces essais sont dans le rapport de programmation

Résultat cas If (cas 1) :

    betterRatioforLPTandMyALGO.txt :
        Ratio :
            Ratio LSA = 1.7142857
            Ratio LPT = 1.1428572
            Ratio MyAlgo = 1.1428572
        Complexity :
         LSA : 0.0021098 secondes.. 0.0018808 secondes. 0.0010827 secondes.
         LPT : 0.0015949 secondes. 0.0011653 secondes. 0.001044 secondes.
         MyAlgo : 0.0025453 secondes.  0.0017055 secondes. 0.0011876 secondes.

    betterComplexityForMyAlgo.txt :
        Ratio :
            Ratio LSA = 1.6
            Ratio LPT = 1.6
            Ratio MyAlgo = 1.6
        Complexity :
         LSA : 8.7E-5 secondes. 0.0018808 secondes. 0.0010827 secondes.
         LPT : 2.63E-4 secondes. 0.0011653 secondes. 0.001044 secondes.
         MyAlgo : 1.231E-4 secondes. 0.0010055 secondes. 0.0011876 secondes.

    SameRatioForAll.txt :

        Ratio :
            Ratio LSA = 1.8
            Ratio LPT = 1.8
            Ratio MyAlgo = 1.8
        Complexity :
         LSA : 9.8E-6 secondes. 7.2E-6 secondes. 1.03E-5 secondes.
         LPT : 6.76E-5 secondes. 5.67E-5 secondes. 7.63E-5 secondes.
         MyAlgo : 1.7E-5 secondes. 2.5E-5 secondes. 2.08E-5 secondes.


Résultat cas 2 : le cas 1 et cas 2 en terme de Ratio et de complexité direct des 3 algorithmes sont les mêmes

Résultat cas Im (cas 3) :

    Pour 100 machines:

        Ratio LSA = 1.4933333
        Ratio LPT = 1.33
        Ratio MyAlgo = 1.33
        Complexité :
             LSA : 3.959E-4 secondes. 7.58E-5 secondes. 8.5E-5 secondes.
             LPT : 7.538E-4 secondes. 2.137E-4 secondes. 3.171E-4 secondes.
             MyAlgo : 8.89E-4 secondes. 4.457E-4 secondes. 7.418E-4 secondes.

    Pour 500 machines :

         Ratio LSA = 1.4986666
         Ratio LPT = 1.3326666
         Ratio MyAlgo = 1.3326666
         Complexité:
              LSA : 6.537E-4 secondes. 9.222E-4 secondes. 5.057E-4 secondes.
              LPT : 0.0074587 secondes. 0.0049059 secondes. 0.004955 secondes.
              MyAlgo : 0.0037638 secondes. 0.001407 secondes. 0.0014587 secondes.

     Pour 5000 machines :
          Ratio LSA: 1.4998667
          Ratio LPT = 1.3326666
          Ratio MyAlgo = 1.3326666
          Complexité:
                 LSA : 0.0462627 secondes. 0.0501468 secondes. 0.0424673 secondes.
                 LPT : 0.0550578 secondes.0.0532094 secondes. 0.0566791 secondes.
                 MyAlgo : 0.1359781 secondes. 0.1396955 secondes. 0.1369782 secondes.

 Résultat cas Ir :

    Pour m=5, n=8, k=4, min=1 et max=14 :
         Ratio moyen LSA = 1.3156744937658915
         Ratio moyen LPT = 1.1347628128542107
         Ratio moyen MyAlgo = 1.1347628128542107
         Total des opérations effectuées en:
          LSA : 2.43E-5 secondes.
          LPT : 1.332E-4 secondes.
          MyAlgo : 1.8E-5 secondes.

    Pour m=100, n=125, k=10, min=1 et max=15 :
        Ratio moyen LSA = 1.8499953073577644
        Ratio moyen LPT = 1.5361634896861698
        Ratio moyen MyAlgo = 1.5361634896861698
        Total des opérations effectuées en:
         LSA : 4.6389999999999995E-4 secondes.
         LPT : 0.0017085999999999998 secondes.
         MyAlgo : 7.968999999999999E-4 secondes.

    Pour m=500, n=700, k=10, min=1, max=15 :
        Ratio moyen LSA = 1.8205310110778812
        Ratio moyen LPT = 1.3345983170806823
        Ratio moyen MyAlgo = 1.3345983170806823
        Total des opérations effectuées en:
         LSA : 0.004681200000000001 secondes.
         LPT : 0.0181338 secondes.
         MyAlgo : 0.008860400000000001 secondes.

    Pour m=500, n=700, k=10, min=1, max=100 (même chose mais tâche possiblement plus grandes) :

        1ère execution :

            Ratio moyen LSA = 1.8320122055131378
            Ratio moyen LPT = 1.4094814323808977
            Ratio moyen MyAlgo = 1.4094814323808977
            Total des opérations effectuées en:
             LSA : 0.009912300000000002 secondes.
             LPT : 0.03175939999999999 secondes.
             MyAlgo : 0.0190852 secondes.

         2ème execution :

            Ratio moyen LSA = 1.8261334629786183
            Ratio moyen LPT = 1.3318410670083654
            Ratio moyen MyAlgo = 1.3318410670083654
            Total des opérations effectuées en:
             LSA : 0.010099200000000001 secondes.
             LPT : 0.0162555 secondes.
             MyAlgo : 0.0178171 secondes.
          pour m=800, n=5000, k=100, min=1 et max=2500:

            Ratio moyen LSA = 1.2030403559573404
            Ratio moyen LPT = 1.007950870262943
            Ratio moyen MyAlgo = 1.007950870262943
            Total des opérations effectuées en:
             LSA : 0.48971339999999985 secondes.
             LPT : 1.0177792 secondes.
             MyAlgo : 2.2051234999999996 secondes.

     idem avec n à 2000:

            Ratio moyen LSA = 1.512557483197013
            Ratio moyen LPT = 1.1077912764219302
            Ratio moyen MyAlgo = 1.1077912764219302
            Total des opérations effectuées en:
             LSA : 0.14797310000000008 secondes.
             LPT : 0.5473689000000003 secondes.
             MyAlgo : 0.45005059999999997 secondes.