﻿# Ratio approximation test for Min-makespan

Groupe : Lucie GIRARD - Erwan DURAND

Ce projet intervient dans le cadre de test de complexité algorithmique pour des algorithmes répondant au problème [MinMakespan](https://en.wikipedia.org/wiki/Job_shop_scheduling)
Le projet propose donc des tests de ratio et de complexité pour les algorithmes LSA, LPT et un algorithme personnel.

Pour compiler le projet : 

	javac src/*.java

Pour lancer le projet :

    cd src
    java Main
	
	
Les données de tests sont disponibles dans le fichier jeuxEssai.txt (avec les résultats associés) et les fichiers .txt de tests pour le 1er cas d'exécution dans le dossier jeuxEssai/casIf.
