import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Algorithm {
    /*
     PROJET DE PROGRAMMATION MIN-MAKESPAN
     Groupe : Lucie GIRARD et Erwan DURAND
     Comme précisé dans le Sujet : les tableaux D sont toujours les durées des taches à affecter aux machines et les tableaux M le temps cumulé pour chaque machines.
      */

    public int LSA(ArrayList<Integer> d, int numberOfMachine){

        ArrayList<Integer> m = new ArrayList<Integer>();
        for(int i=0; i<numberOfMachine; i++){
            m.add(0);
        }
        int minMachine = 0;
        while(!d.isEmpty()){
            m.set(minMachine,m.get(minMachine) + d.get(0));
            d.remove(0);
            minMachine = nextMachine(m);
            //System.out.println("D : "+d);
            //System.out.println("M : "+m);
        }
        return Collections.max(m);
    }

    public int LPT(ArrayList<Integer> d, int numberOfMachine){
        Collections.sort(d, Collections.reverseOrder());
        System.out.println(d);
        return LSA(d,numberOfMachine);
    }

    private int nextMachine(ArrayList<Integer> list){
        int min = list.get(0);
        int res = 0;
        for(int i=1; i<list.size(); i++){
            int tempo = list.get(i);
            if( tempo < min){
                min = tempo;
                res = i;
            }
        }
        return res;
    }

    public int MyAlgo(ArrayList<Integer> d, int numberOfMachine){
        ArrayList<Integer> m = new ArrayList<Integer>();
        for(int i=0; i<numberOfMachine; i++){
            m.add(0);
        }
        int minMachine = 0;
        int max;
        while(!d.isEmpty()){
            max = Collections.max(d); // on fait un max à chaque itération et pas de tri au début
            m.set(minMachine,m.get(minMachine) + max);
            d.remove(d.indexOf(max));
            minMachine = nextMachine(m);
          //  System.out.println("D : "+d);
           // System.out.println("M : "+m);
        }
        return Collections.max(m);
    }

}
