import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.*;

public class Main {

    /*
     PROJET DE PROGRAMMATION MIN-MAKESPAN
     Groupe : Lucie GIRARD et Erwan DURAND
     Comme précisé dans le Sujet : les tableaux d sont toujours les durées des taches à affecter aux machines.
      */
    public static ArrayList<Integer> donneesTest(){
        ArrayList<Integer> d = new ArrayList<>();
        d.add(5);
        d.add(6);
        d.add(2);
        d.add(4);
        d.add(3);
        d.add(7);
        d.add(8);
        return d;
        //3:7:5:6:2:4:3:7:8
    }

    // menu pour que l'utilisateur choisisse le mode d'entrée des instances
    public static int menu() {

        int selection;
        Scanner input = new Scanner(System.in);
            /***************************************************/

            System.out.println("Choissisez une instance d'entrée");
            System.out.println("-------------------------\n");
            System.out.println("1 - If (Fichier) ");
            System.out.println("2 - Ic (Clavier) ");
            System.out.println("3 - Im");
            System.out.println("4 - Ir (Random)");
            System.out.println("5 - Quitter");
        selection = input.nextInt();
        return selection;

    }

    // affiche les résultats pour le cas 1, 2 et 3
    public static void prodResultDisplay(ArrayList<Integer> d,int numberOfMachine){
        long tempsDebut;
        long tempsFin;
        double secondsLSA;
        double secondsLPT;
        double secondsMyALGO;

        Algorithm algo = new Algorithm();
        int borneMax = Collections.max(d);
        int sommeDuree = 0;
        for(int i=0; i<d.size(); i++) {
            sommeDuree += d.get(i);
        }
        int borneAVG = sommeDuree / numberOfMachine;
        int maxb = borneAVG > borneMax ? borneAVG : borneMax;
        tempsDebut = System.nanoTime();
        int resultLSA = algo.LSA(new ArrayList<>(d),numberOfMachine);
        tempsFin = System.nanoTime();
        secondsLSA = (tempsFin - tempsDebut) / 1e9;
        float ratioLSA = (float) resultLSA / maxb;
        tempsDebut = System.nanoTime();
        int resultLPT = algo.LPT(new ArrayList<>(d),numberOfMachine);
        tempsFin = System.nanoTime();
        secondsLPT = (tempsFin - tempsDebut) / 1e9;
        float ratioLPT = (float) resultLPT / maxb;
        tempsDebut = System.nanoTime();
        int resultMyAlgo = algo.MyAlgo(new ArrayList<>(d),numberOfMachine);
        tempsFin = System.nanoTime();
        secondsMyALGO = (tempsFin - tempsDebut) / 1e9;
        float ratioMyAlgo = (float) resultMyAlgo / maxb;

        System.out.println("----------------------------- RESULTAT -----------------------------");
        System.out.println("Borne infèrieure ''maximum'' = "+borneMax);
        System.out.println("Borne infèrieure ''moyenne'' = "+borneAVG);
        System.out.println("Résultat LSA = "+resultLSA);
        System.out.println("Ratio LSA = "+ratioLSA);
        System.out.println("Résultat LPT = "+resultLPT);
        System.out.println("Ratio LPT = "+ratioLPT);
        System.out.println("Résultat MyAlgo = "+resultMyAlgo);
        System.out.println("Ratio MyAlgo = "+ratioMyAlgo);
        System.out.println("Opération effectuée en:\n LSA : "+ Double.toString(secondsLSA) + " secondes. \n LPT : "+ Double.toString(secondsLPT) + " secondes. \n MyAlgo : "+ Double.toString(secondsMyALGO) + " secondes. \n  ");

    }

    public static void main(String[] args) throws FileNotFoundException {
/*
        Algorithm algo = new Algorithm();
        System.out.println("----------------   LSA   ----------------");
        int lsaResult = algo.LSA(donneesTest(),3);
        System.out.println("----------------   LPT   ----------------");
        int lptResult = algo.LPT(donneesTest(),3);
        System.out.println("----------------   MyAlgo   ----------------");
        int myalgoResult = algo.MyAlgo(donneesTest(),3);
        System.out.println("Result LSA  : "+lsaResult);
        System.out.println("Result LPT  : "+lptResult);
        System.out.println("Result MYALGO  : "+lptResult);
*/
        Algorithm algo = new Algorithm();
        int instanceNum = 0;
        int prodResultatNum = 0;
        int m;
        int n;
        int k;
        int min;
        int max;
        long tempsDebut;
        long tempsFin;
        double secondsLSA = 0;
        double secondsLPT = 0;
        double secondsMyALGO = 0;
        boolean arret = false;
        while (!arret){
            instanceNum = menu();
            ArrayList<Integer> d = new ArrayList<Integer>();
            // Switch sur ce qu'a choisi l'utilisateur
            switch(instanceNum)
            {
                // fichier
                case 1 :{
                    String currentFolder = System.getProperty("user.dir");
                    String fichier;
                    Scanner input = new Scanner(System.in);
                    System.out.println("Entrez le nom du fichier :");
                    fichier = input.nextLine();
                    File text = new File(currentFolder+"/jeuxEssai/casIf/"+fichier);
                    Scanner scnr = new Scanner(text);
                    String line = scnr.nextLine();
                    String separate[] = line.split(":");
                    m = Integer.parseInt(separate[0]);
                    n = Integer.parseInt(separate[1]);
                    for(int i=2; i<n+2; i++){
                        d.add(Integer.parseInt(separate[i]));
                    }
                    prodResultDisplay(d,m);
                } break;

                // chaine entrée au clavier
                case 2 :{
                    String clavier;
                    Scanner input = new Scanner(System.in);
                    System.out.println("Entrez la chaine : ");
                    clavier = input.nextLine();
                    String separate[] = clavier.split(":");
                    m = Integer.parseInt(separate[0]);
                    n = Integer.parseInt(separate[1]);
                    for(int i=2; i<n+2; i++){
                        d.add(Integer.parseInt(separate[i]));
                    }
                    prodResultDisplay(d,m);
                } break;

                // Génération d'un cas d'instance de type Im (au dessus de la question 8 du sujet)
                case 3 :{
                    Scanner input = new Scanner(System.in);
                    System.out.println("Entrez le nombre de machine :");
                    m = input.nextInt();
                    int duree = m;
                    d.add(duree);d.add(duree);d.add(duree);
                    while(duree < (m+m-1)){
                        duree++;
                        d.add(duree);
                        d.add(duree);
                    }
                    prodResultDisplay(d,m);
                } break;

                // Génération aléatoire de k instances
                case 4 :{
                    Scanner input = new Scanner(System.in);
                    System.out.println("Entrez le nombre de machine :");
                    m = input.nextInt();
                    System.out.println("Entrez le nombre de tâches :");
                    n = input.nextInt();
                    System.out.println("Entrez le nombre d'instance k, à générer :");
                    k = input.nextInt();
                    System.out.println("Entrez le minimum des durées :");
                    min = input.nextInt();
                    System.out.println("Entrez le maximum des durées :");
                    max = input.nextInt();

                    Double borneAVG;
                    int resultLSA;
                    Double ratioLSA;
                    int resultLPT;
                    Double ratioLPT;
                    int resultMyAlgo;
                    double maxb;
                    int borneMax;
                    Double ratioMyAlgo;
                    secondsLSA = 0;
                    secondsLPT = 0;
                    secondsMyALGO = 0;
                    int randNumber;
                    Random random = new Random();
                    ArrayList<Double> lsaRatioList = new ArrayList<Double>();
                    ArrayList<Double> lptRatioList = new ArrayList<Double>();
                    ArrayList<Double> myalgoRatioList = new ArrayList<Double>();
                    for (int i=0; i<k; i++){
                        ArrayList<Integer> instance = new ArrayList<>();
                        for(int j=0; j<n; j++){
                            randNumber = min+random.nextInt((max+1)-min);
                            instance.add(randNumber);
                        }

                        // maintenant qu'on à généré les tâches de façon aléatoire, on calcul les ratios
                        int sommeDuree = 0;
                        for(int w=0; w<instance.size(); w++){
                            sommeDuree += instance.get(w);
                        }
                        //LSA
                        borneMax = Collections.max(instance);
                        borneAVG = ((double)sommeDuree) / m;
                        maxb = borneAVG > borneMax ? borneAVG : borneMax;
                        tempsDebut = System.nanoTime();
                        resultLSA = algo.LSA(new ArrayList<>(instance),m);
                        tempsFin = System.nanoTime();
                        secondsLSA += (tempsFin - tempsDebut) / 1e9;
                        ratioLSA = ((double) resultLSA) / maxb;
                        //LPT
                        tempsDebut = System.nanoTime();
                        resultLPT = algo.LPT(new ArrayList<>(instance),m);
                        tempsFin = System.nanoTime();
                        secondsLPT += (tempsFin - tempsDebut) / 1e9;
                        ratioLPT = ((double) resultLPT) / maxb;
                        //MyAlgo
                        tempsDebut = System.nanoTime();
                        resultMyAlgo = algo.MyAlgo(new ArrayList<>(instance),m);
                        tempsFin = System.nanoTime();
                        secondsMyALGO += (tempsFin - tempsDebut) / 1e9;
                        ratioMyAlgo =  ((double) resultMyAlgo) / maxb;

                        lsaRatioList.add(ratioLSA);
                        lptRatioList.add(ratioLPT);
                        myalgoRatioList.add(ratioMyAlgo);
                    }
                    // maintenant qu'on à tous les ratios, on fait des moyennes
                    Double avgLSA;
                    Double avgLPT;
                    Double avgMyALGO;
                    Double total = 0.0;
                    for(int i=0; i<lsaRatioList.size(); i++){
                        total = total + lsaRatioList.get(i);
                    }
                    avgLSA = total / lsaRatioList.size();

                    total = 0.0;
                    for(int i=0; i<lptRatioList.size(); i++){
                        total = total + lptRatioList.get(i);
                    }
                    avgLPT = total / lptRatioList.size();

                    total = 0.0;
                    for(int i=0; i<myalgoRatioList.size(); i++){
                        total = total + myalgoRatioList.get(i);
                    }
                    avgMyALGO = total / myalgoRatioList.size();



                    System.out.println("----------------------------- RESULTAT -----------------------------");
                    System.out.println("Ratio moyen LSA = "+avgLSA);
                    System.out.println("Ratio moyen LPT = "+avgLPT);
                    System.out.println("Ratio moyen MyAlgo = "+avgMyALGO);
                    System.out.println("Total des opérations effectuées en:\n LSA : "+ Double.toString(secondsLSA) + " secondes. \n LPT : "+ Double.toString(secondsLPT) + " secondes. \n MyAlgo : "+ Double.toString(secondsMyALGO) + " secondes. \n  ");
                } break;

                case 5 : arret = true; break;
                default : System.out.println("entrez un choix entre 1 et 5"); break;
            }
        }
    }
}

